require 'jwt'
require 'bcrypt'

class AuthenticationService
  include BCrypt

  def self.password_hash pass
    Password.create pass
  end

  def self.get_password pass
    Password.new pass
  end

  def self.create_token user
    payload = {
      id: user[:id],
      iat: Time.now.to_i,
      exp: Time.now.to_i + 1296000
    }
    JWT.encode payload, @hmac_secret, 'HS256'
  end

  def self.decoded_token token
    JWT.decode token, @hmac_secret, true, { :algorithm => 'HS256' }
  end

  def self.user_login user, params
    user_response = user.find_by_email(params[:email])
    if !user_response.nil? && get_password(user_response["password"]) == params[:password]
      return {token: 'Bearer ' + create_token(user_response)}
    else
      return {err: 'User not found', code: '004'}
    end
  end

  def self.set_user user, header_token
    token = get_user_data(header_token)
    if token["id"].is_a? Integer
      return user.find(token["id"])
    else
      return token
    end
  end

  def self.verify_user_params user, params
    if user.find_by_email params[:email]
      return {err: 'Email already used', code: '002'}
    end
    if user.find_by phone: params[:phone]
      return {err: 'Phone number already used', code: '003'}
    end
  end

  def self.get_user_data header_token
    begin
      token = decoded_token(header_token.split(" ")[1])[0]
      if token["exp"] > Time.now.to_i
        return token
      else
        return {err: 'Token expired', code: '006'}
      end
    rescue Exception
      return {err: 'Token not valid', code: '005'}
    end
  end

  private
    @hmac_secret = 'CrEaTinG$SeCuRe%PaSsWoRd.6692"5f1da/83c5(4354da73d81?e0+13974d'
end
