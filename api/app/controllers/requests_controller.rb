class RequestsController < ApplicationController
  before_action :set_user
  before_action :is_teacher, only: [:search, :join]
  before_action :is_student, only: [:index, :create, :update, :destroy]
  before_action :set_request, only: [:show, :update, :destroy]

  # GET /requests
  def index
    render json: @user.requests
  end

  # POST /requests
  def search
    render json: Request.where(search_params)
  end

  # GET /requests/:id
  def show
    render json: @request
  end

  # POST /requests/:id/accept
  def accept
    # Course.new({@request["title"]})
  end

  # POST /request
  def create
    @request = Request.new(request_params)

    if @request.save
      # @user.requests << @request
      render json: @request, status: :created, location: @request
    else
      render json: @request.errors, status: :unprocessable_entity
    end
  end

  # PUT /request
  def update
    if @request.update(request_params)
      render json: @request
    else
      render json: @request.errors, status: :unprocessable_entity
    end
  end

  # DELETE /request
  def destroy
    @request.destroy
  end

  private
    def set_user
      @user = AuthenticationService.set_user User, request.headers["Authorization"]
      if !@user.is_a?(User)
        render json: @user
      end
    end

    def set_request
      @request = Request.find(params[:id])
    end

    def request_params
      params.permit(:title, :description, :date, :duration, :tags).merge(status: 'pending', user: @user)
    end

    def search_params
      params.permit(:title, :description, :date, :duration, :tags)
    end
end
