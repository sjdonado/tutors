class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]
  before_action :verify_params, only: :create

  # GET /user
  def show
    render json: @user
  end

  # POST /user
  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # POST /user/login
  def login
    render json: AuthenticationService.user_login(User, params)
  end

  # PUT /user
  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /user
  def destroy
    @user.destroy
  end

  private
    def set_user
      @user = AuthenticationService.set_user User, request.headers["Authorization"]
      if !@user.is_a?(User)
        render json: @user
      end
    end

    def verify_params
      result = AuthenticationService.verify_user_params(User, params)
      if !result.nil?
        render json: result
      end
    end

    def user_params
      params.permit(:first_name, :last_name, :email, :role, :phone, :password, :studies).merge(password: AuthenticationService.password_hash(params[:password]), coins: 0, score: 0, comments: "")
    end
end
