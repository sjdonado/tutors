class ApplicationController < ActionController::API
  def is_student
    unless @user["role"] == "student"
      render json: {err: 'Student not found', code: "007"}
    end
  end

  def is_teacher
    unless @user["role"] == "teacher"
      render json: {err: 'Teacher not found', code: "007"}
    end
  end
end
