class CoursesController < ApplicationController
  before_action :set_user
  before_action :is_teacher, only: [:create, :update, :destroy]
  before_action :set_course, only: [:show, :join_course, :update, :destroy]

  # GET /courses
  def index
    render json: @user.courses
  end

  # POST /courses/search
  def search
    render json: Course.where("title like ?", "#{search_params[:title]}%")
  end

  # GET /courses/:id
  def show
    render json: {course: @course, users: @course.users}
  end

  # POST /course/:id/join
  def join_course
    if @course.users.exists? @user["id"]
      render json: {err: 'Already joined', code: '008'}
    else
      if @user["coins"] < @course["price"]
        render json: {err: 'Insufficient coins', code: '009'}
      else
        if @user.update({coins: @user["coins"] - @course["price"]})
          if Transaction.new({user: @user, coins: @course["price"], status: 'completed', transaction_code: "#{join_params[:title].upcase}", kind: 1}).save
            puts "Estoy en el if y este es el user_id #{@user.id} y el nuevo curso es  #{@course.id}"
            @user.courses << @course
            render json: @course
          else
            render json: {err: 'Error saving transaction', code: '011'}
          end
        else
          render json: {err: 'Error geting user coins', code: '010'}
        end
      end
    end
  end

  # POST /course
  def create
    @course = Course.new(course_params)

    if @course.save
      @user.courses << @course
      render json: @course, status: :created, location: @course
    else
      render json: @course.errors, status: :unprocessable_entity
    end
  end

  # PUT /course
  def update
    if @course.update(course_params)
      render json: @course
    else
      render json: @course.errors, status: :unprocessable_entity
    end
  end

  # DELETE /course
  def destroy
    @course.destroy
  end

  private
    def set_user
      @user = AuthenticationService.set_user User, request.headers["Authorization"]
      if !@user.is_a?(User)
        render json: @user
      end
    end

    def set_course
      @course = Course.find(params[:id])
    end

    def course_params
      params.permit(:title, :capacity, :price, :place, :duration, :date, :tags).merge(status: 'new')
    end

    def search_params
      params.permit(:title, :capacity, :price, :place, :duration, :date, :tags)
    end

    def join_params
      params.permit(:title)
    end
end
