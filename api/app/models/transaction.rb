class Transaction < ApplicationRecord
  validates_presence_of :coins, :status, :transaction_code, :kind
  belongs_to :user
end
