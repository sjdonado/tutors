class Course < ApplicationRecord
  validates_presence_of :title, :capacity, :price, :place, :duration, :date, :tags
  has_and_belongs_to_many :users
end
