class Request < ApplicationRecord
  validates_presence_of :title, :description, :date, :duration, :tags, :status
  belongs_to :user
end
