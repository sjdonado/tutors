class User < ApplicationRecord

  # Validations
  validates_presence_of :first_name, :last_name, :email, :role, :phone, :password, :studies

  # Realationship
  has_and_belongs_to_many :courses
  has_many :requests
  has_many :transactions

  # Callbacks
  # before_save :format_attribitues

  # Methods
  # private
  # def format_attribitues
  #   self.name.downcase!
  # end
end
