class ApplicationRecord < ActiveRecord::Base
  enum role: { student: 0, teacher: 1, admin: 2 }
  enum kind: { payco: 0, joined_to_course: 1 }

  self.abstract_class = true
end
