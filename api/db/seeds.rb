require 'faker'

@courses = Array.new
@teachers = Array.new

def create_users type
  10.times do
    user = User.create(first_name: Faker::Name.first_name , last_name: Faker::Name.last_name, email: Faker::Internet.email, role: type, phone: Faker::Number.number(10), password: AuthenticationService.password_hash('12345'), studies: Faker::University.name, coins: 0, comments: '', score: 0)
    if type == 1
      @teachers << user
    end
  end
end

def create_courses
  10.times do
    now = Time.now.to_i
    last_date = (Time.now + 60 * 60 * 60 * 24).to_i
    date = Time.at(rand(now..last_date))
    tags = "#{Faker::ProgrammingLanguage.name}, #{Faker::ProgrammingLanguage.name}, #{Faker::ProgrammingLanguage.name}"
    @courses << Course.create(title: Faker::Name.title, capacity: Faker::Number.between(1, 5), price: Faker::Number.between(30, 60), place: Faker::University.name, duration: Faker::Number.between(30, 180), date: date, tags: tags, status: 'new')
  end
end

# Create courses
create_courses

# Create teachers
User.create(first_name: Faker::Name.first_name , last_name: Faker::Name.last_name, email: 'teacher@test.com', role: 1, phone: Faker::Number.number(10), password: AuthenticationService.password_hash('12345'), studies: Faker::University.name, coins: 0, comments: '', score: 0)
create_users 1

i = 0
10.times do
  @teachers[i].courses << @courses[i]
  puts @courses[i]
  i += 1
end

# Create students
create_users 0
User.create(first_name: Faker::Name.first_name , last_name: Faker::Name.last_name, email: 'juan@test.com', role: 0, phone: Faker::Number.number(10), password: AuthenticationService.password_hash('12345'), studies: Faker::University.name, coins: 0, comments: '', score: 0)
User.create(first_name: Faker::Name.first_name , last_name: Faker::Name.last_name, email: 'roberto@test.com', role: 0, phone: Faker::Number.number(10), password: AuthenticationService.password_hash('12345'), studies: Faker::University.name, coins: 0, comments: '', score: 0)
User.create(first_name: Faker::Name.first_name , last_name: Faker::Name.last_name, email: 'cabarcas@test.com', role: 0, phone: Faker::Number.number(10), password: AuthenticationService.password_hash('12345'), studies: Faker::University.name, coins: 0, comments: '', score: 0)
