class CreateCourses < ActiveRecord::Migration[5.1]
  def change
    create_table :courses do |t|
      t.string :title, limit: 255
      t.integer :capacity, limit: 2
      t.integer :price, limit: 3
      t.string :place, limit: 255
      t.integer :duration, limit: 3
      t.datetime :date
      t.text :tags
      t.string :status, limit: 15
      t.timestamps
    end
  end
end
