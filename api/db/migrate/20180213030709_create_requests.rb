class CreateRequests < ActiveRecord::Migration[5.1]
  def change
    create_table :requests do |t|
      t.references :user, foreign_key: true
      t.string :title, limit: 50
      t.text :description
      t.datetime :date
      t.integer :duration, limit: 3
      t.text :tags
      t.string :status, limit: 15
      t.timestamps
    end
  end
end
