class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :first_name, limit: 255
      t.string :last_name, limit: 255
      t.string :email, limit: 255
      t.integer :role
      t.integer :phone, limit: 10
      t.string :password, limit: 255
      t.integer :coins, limit: 3
      t.text :studies
      t.integer :score, limit: 3
      t.text :comments
      t.timestamps
    end
  end
end
