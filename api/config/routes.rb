Rails.application.routes.draw do
  # Users
  post 'users/login', to: 'users#login'
  post 'users', to: 'users#create'
  get 'users', to: 'users#show'
  put 'users', to: 'users#update'
  delete 'users', to: 'users#destroy'
  # Courses
  post 'courses/search', to: 'courses#search'
  post 'courses/:id/join', to: 'courses#join_course'
  resources :courses
  # Requests
  post 'requests/search', to: 'requests#search'
  post 'request/:id/accept', to: 'requests#accept'
  resources :requests
  # Transactions
  post 'transactions/search', to: 'transactions#search'
  get 'transactions/:id', to: 'transactions#show'
  post 'transactions', to: 'transactions#create'
  put 'transactions', to: 'transactions#update'
end
