package com.projects.juan.tutors.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.projects.juan.tutors.R;
import com.projects.juan.tutors.adapters.CoursesAdapter;
import com.projects.juan.tutors.models.Course;
import com.projects.juan.tutors.modules.HttpRequests;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

/**
 * A simple {@link Fragment} subclass.
 */
public class CoursesFragment extends Fragment {

    private static ArrayList<Course> courses = new ArrayList<>();
    private CoursesAdapter adapter;
    private SwipeRefreshLayout refreshLayout;

    public CoursesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        if(courses.isEmpty()) getCourses();
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_courses, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        Toast.makeText(getContext(), token, Toast.LENGTH_LONG).show();
        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_courses);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getCourses();
            }
        });
        //Instance and recycler
        final RecyclerView recyclerView = view.findViewById(R.id.recycler_courses);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
//        layoutManager.scrollToPosition(notes.size() - 1);
//        recyclerView.scrollToPosition(notes.size() - 1);
        recyclerView.setLayoutManager(layoutManager);

//        Instance and config adapter
        adapter = new CoursesAdapter(courses, R.layout.recycler_view_course_item, new CoursesAdapter.OnClickListener() {
            @Override
            public void onClick(Course course, TextView teachers, TextView students) {

            }
        }, new CoursesAdapter.OnLongClickListener() {
            @Override
            public void onLongClick(Course course) {

            }
        });
        recyclerView.setAdapter(adapter);

    }

    private void getCourses (){
        HttpRequests.getRequest(getContext(), getArguments().getString("token"), getResources().getString(R.string.GET_COURSES), "Network error, try again", new HttpRequests.CallBack(){
            @Override
            public void sendResponse(String response) {
            try {
                JSONArray courses_response = new JSONArray(response);
                courses.clear();
                for(int i = 0; i < courses_response.length(); i++){
                    JSONObject cr = courses_response.getJSONObject(i);
                    courses.add(new Course(cr.getInt("id"), cr.getString("title"), cr.getString("price"), cr.getString("place"), cr.getInt("duration"), cr.getString("date"), cr.getString("tags")));
                }
                Collections.reverse(courses);
                adapter.notifyDataSetChanged();
                refreshLayout.setRefreshing(false);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            }
        });
    }
}
