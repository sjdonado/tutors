package com.projects.juan.tutors.models;

/**
 * Created by juan on 13/02/18.
 */

public class Course {
    private int id;
    private String title;
    private String price;
    private String place;
    private int duration;
    private String date;
    private String tags;

    public Course(int id, String title, String price, String place, int duration, String date, String tags) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.place = place;
        this.duration = duration;
        this.date = date;
        this.tags = tags;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getPrice() {
        return price;
    }

    public String getPlace() {
        return place;
    }

    public int getDuration() {
        return duration;
    }

    public String getDate() {
        return date;
    }

    public String getTags() {
        return tags;
    }
}
