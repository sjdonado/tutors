package com.projects.juan.tutors.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.projects.juan.tutors.R;
import com.projects.juan.tutors.models.Course;

import java.util.ArrayList;

/**
 * Created by juan on 13/02/18.
 */

public class CoursesAdapter extends RecyclerView.Adapter<CoursesAdapter.ViewHolder>{

    private Context context;
    private ArrayList<Course> courses;
    private int layout;
    private OnLongClickListener longClickListener;
    private OnClickListener clickListener;

    public CoursesAdapter(ArrayList<Course> courses, int layout, OnClickListener clickListener, OnLongClickListener longClickListener) {
        this.courses = courses;
        this.layout = layout;
        this.longClickListener = longClickListener;
        this.clickListener = clickListener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(courses.get(position), clickListener, longClickListener);
    }

    @Override
    public int getItemCount() {
        return courses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView date;
        public TextView place;
        public TextView duration;
        public TextView tags;
        public TextView teachers;
        public TextView students;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title_course);
            date = (TextView) itemView.findViewById(R.id.date_course);
            place = (TextView) itemView.findViewById(R.id.place_course);
            duration = (TextView) itemView.findViewById(R.id.duration_course);
            tags = (TextView) itemView.findViewById(R.id.tags_course);
            teachers = (TextView) itemView.findViewById(R.id.teachers_course);
            students = (TextView) itemView.findViewById(R.id.students_course);
        }

        public void bind(final Course course, final OnClickListener clickListener, final OnLongClickListener longClickListener){

            title.setText(course.getTitle());
            date.setText(course.getDate());
            place.setText("Place: " + course.getPlace());
            duration.setText("Duration: " + course.getDuration());
            tags.setText("Tags: " + course.getTags());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onClick(course, teachers, students);
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    longClickListener.onLongClick(course);
                    return true;
                }
            });
        }
    }

    public interface OnLongClickListener{
        void onLongClick(Course course);
    }

    public interface OnClickListener{
        void onClick(Course course, TextView teachers, TextView students);
    }

}
