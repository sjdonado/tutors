package com.projects.juan.tutors.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.projects.juan.tutors.R;
import com.projects.juan.tutors.adapters.TransactionsAdapter;
import com.projects.juan.tutors.models.Transaction;
import com.projects.juan.tutors.modules.HttpRequests;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

/**
 * A simple {@link Fragment} subclass.
 */
public class TransactionsFragment extends Fragment {

    private static ArrayList<Transaction> transactions = new ArrayList<>();
    private TransactionsAdapter transactionsAdapter;
    private SwipeRefreshLayout refreshLayout;


    public TransactionsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onStart() {
        if(transactions.isEmpty()) getTransactions(new JSONObject());
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transactions, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_search_transactions);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getTransactions(new JSONObject());
            }
        });

        final RecyclerView recyclerView = view.findViewById(R.id.recycler_search_transactions);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        transactionsAdapter = new TransactionsAdapter(transactions, R.layout.recycler_view_transaction_item, new TransactionsAdapter.OnClickListener() {
            @Override
            public void onClick(Transaction transaction) {
                getTransactions(new JSONObject());
            }
        });
        recyclerView.setAdapter(transactionsAdapter);
    }

    private void getTransactions(JSONObject search){
        HttpRequests.postRequest(getContext(), getArguments().getString("token"), getResources().getString(R.string.GET_SEARCH_TRANSACTIONS), search, "Transactions not found", new HttpRequests.CallBack() {
            @Override
            public void sendResponse(String response) {
                try {
                    JSONArray transactions_response = new JSONArray(response);
                    transactions.clear();
                    for(int i = 0; i < transactions_response.length(); i++){
                        JSONObject tr = transactions_response.getJSONObject(i);
                        transactions.add(new Transaction(tr.getInt("id"),  tr.getInt("coins"), tr.getString("transaction_code"), tr.getString("status"), tr.getString("kind")));
                    }
                    Collections.reverse(transactions);
                    transactionsAdapter.notifyDataSetChanged();
                    refreshLayout.setRefreshing(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
