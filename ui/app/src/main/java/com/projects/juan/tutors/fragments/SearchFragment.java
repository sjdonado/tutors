package com.projects.juan.tutors.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.projects.juan.tutors.R;
import com.projects.juan.tutors.adapters.CoursesAdapter;
import com.projects.juan.tutors.models.Course;
import com.projects.juan.tutors.modules.HttpRequests;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {

    private static ArrayList<Course> courses = new ArrayList<>();
    private CoursesAdapter adapter;
    private SwipeRefreshLayout refreshLayout;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        if(courses.isEmpty()) getCourses(new JSONObject());
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_search_courses);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getCourses(new JSONObject());
            }
        });

        final EditText edit_search = (EditText) view.findViewById(R.id.edit_search_courses);

        edit_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                JSONObject search_object = new JSONObject();
                try {
                    search_object.put("title", edit_search.getText());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                getCourses(search_object);
            }
        });

        final RecyclerView recyclerView = view.findViewById(R.id.recycler_search_courses);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
//        layoutManager.scrollToPosition(notes.size() - 1);
//        recyclerView.scrollToPosition(notes.size() - 1);
        recyclerView.setLayoutManager(layoutManager);

//        Instance and config adapter
        adapter = new CoursesAdapter(courses, R.layout.recycler_view_course_item, new CoursesAdapter.OnClickListener() {
            @Override
            public void onClick(Course course, final TextView teachers, final TextView students) {
                HttpRequests.getRequest(getContext(), getArguments().getString("token"), getResources().getString(R.string.GET_COURSES) + course.getId(), "Course details not found", new HttpRequests.CallBack() {
                    @Override
                    public void sendResponse(String response) {
                        try {
                            JSONArray users = new JSONObject(response).getJSONArray("users");
                            String teachersArray = "";
                            String studentsArray = "";
                            for (int i = 0; i < users.length(); i++){
                                JSONObject user = new JSONObject(users.get(i).toString());
                                if(user.getString("role").equals("teacher")) teachersArray += user.getString("first_name") + " " + user.getString("last_name") + ",";
                                if(user.getString("role").equals("student")) studentsArray += user.getString("first_name") + " " + user.getString("last_name") + ",";
                            }
                            teachers.setText("Teachers: " + teachersArray);
                            students.setText("Students: " + studentsArray);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }, new CoursesAdapter.OnLongClickListener() {
            @Override
            public void onLongClick(final Course course) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Join to " + course.getTitle());
                builder.setMessage("Price: " + course.getPrice());

                builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        JSONObject join_params = new JSONObject();
                        try {
                            join_params.put("title", course.getTitle());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        HttpRequests.postRequest(getContext(), getArguments().getString("token"),
                                getResources().getString(R.string.GET_COURSES) + course.getId() + "/join", join_params, "Network error", new HttpRequests.CallBack() {
                                    @Override
                                    public void sendResponse(String response) {
                                        try {
                                            JSONObject jr = new JSONObject(response);
                                            if(response.contains("err")){
                                                Log.d("err", response);
                                                Toast.makeText(getContext(), jr.getString("err"), Toast.LENGTH_LONG).show();
                                            }else{
                                                Toast.makeText(getContext(), "Joined to " + course.getTitle() + " successfully", Toast.LENGTH_LONG).show();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                    }
                });
                builder.setNegativeButton("Cancel", null);

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void getCourses(JSONObject search){
        HttpRequests.postRequest(getContext(), getArguments().getString("token"), getResources().getString(R.string.GET_SEARCH_COURSES), search, "Courses not found", new HttpRequests.CallBack() {
            @Override
            public void sendResponse(String response) {
                try {
                    JSONArray courses_response = new JSONArray(response);
                    courses.clear();
                    for(int i = 0; i < courses_response.length(); i++){
                        JSONObject cr = courses_response.getJSONObject(i);
                        courses.add(new Course(cr.getInt("id"), cr.getString("title"), cr.getString("price"), cr.getString("place"), cr.getInt("duration"), cr.getString("date"), cr.getString("tags")));
                    }
                    Collections.reverse(courses);
                    adapter.notifyDataSetChanged();
                    refreshLayout.setRefreshing(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
