package com.projects.juan.tutors.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.projects.juan.tutors.R;
import com.projects.juan.tutors.modules.HttpRequests;

import org.json.JSONException;
import org.json.JSONObject;

public class AuthActivity extends AppCompatActivity {

    @Override
    protected void onStart() {
        SharedPreferences sharedPreferences = getSharedPreferences("user_pref", getApplicationContext().MODE_PRIVATE);
        String token = sharedPreferences.getString("token", null);
        if(token != null && !token.isEmpty()){
            Intent authSuccessful = new Intent(getBaseContext(), MainActivity.class);
            startActivity(authSuccessful);
        }
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        final EditText email = (EditText) findViewById(R.id.edit_email);
        final EditText pass = (EditText) findViewById(R.id.edit_pass);

        findViewById(R.id.btn_signin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            JSONObject request_params = new JSONObject();
            try {
                request_params.put("email", email.getText().toString());
                request_params.put("password", pass.getText().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            HttpRequests.postRequest(getBaseContext(), null, getResources().getString(R.string.LOGIN), request_params, "Network error, try again", new HttpRequests.CallBack(){
                @Override
                public void sendResponse(String response) {
                    if(response.contains("err")){
                        Toast.makeText(getApplicationContext(), "User not found", Toast.LENGTH_LONG).show();
                    }else{
                        SharedPreferences sharedPreferences = getSharedPreferences("user_pref", getApplicationContext().MODE_PRIVATE);
                        SharedPreferences.Editor writer = sharedPreferences.edit();
                        try {
                            writer.putString("token", new JSONObject(response).getString("token"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        writer.commit();

                        Intent authSuccessful = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(authSuccessful);
                    }
                }
            });
            }
        });
    }
}
