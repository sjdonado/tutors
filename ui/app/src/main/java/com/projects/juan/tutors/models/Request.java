package com.projects.juan.tutors.models;

/**
 * Created by juan on 14/02/18.
 */

public class Request {
    private int id;
    private String title;
    private String description;
    private String date;
    private int duration;
    private String tags;
    private String status;

    public Request(int id, String title, String description, String date, int duration, String tags, String status) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.date = date;
        this.duration = duration;
        this.tags = tags;
        this.status = status;
    }
}
