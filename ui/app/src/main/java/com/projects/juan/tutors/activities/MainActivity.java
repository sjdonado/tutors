package com.projects.juan.tutors.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.projects.juan.tutors.R;
import com.projects.juan.tutors.fragments.CoursesFragment;
import com.projects.juan.tutors.fragments.PofileFragment;
import com.projects.juan.tutors.fragments.RequestsFragment;
import com.projects.juan.tutors.fragments.SearchFragment;
import com.projects.juan.tutors.fragments.TransactionsFragment;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private Bundle bundle;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            final android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_search:
                    setFragment(transaction, new SearchFragment());
                    return true;
                case R.id.navigation_courses:
                    setFragment(transaction, new CoursesFragment());
                    return true;
                case R.id.navigation_requests:
                    setFragment(transaction, new RequestsFragment());
                    return true;
                case R.id.navigation_transactions:
                    setFragment(transaction, new TransactionsFragment());
                    return true;
                case R.id.navigation_profile:
                    setFragment(transaction, new PofileFragment());
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_courses);
    }

    private void setFragment(android.support.v4.app.FragmentTransaction transaction, Fragment fragment){
        sharedPreferences = getSharedPreferences("user_pref", getApplicationContext().MODE_PRIVATE);
        bundle = new Bundle();
        bundle.putString("token", sharedPreferences.getString("token", null));
        fragment.setArguments(bundle);
        transaction.replace(R.id.fragment_container, fragment).commit();
    }


}
